# terraform-ansible-inventory demo stage 2

Modify stage 1 with customized AMIs build with packer

### bastion

AMI has ansible installed

### node

AMI has python installed as a dependency for being an
ansible target host
