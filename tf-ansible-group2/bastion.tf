resource "aws_instance" "bastion" {
  count = "${var.bastion_count}"
  ami = "ami-a5c7c7b3"
  instance_type = "t2.micro"
  key_name = "wadeITTLPC"
  subnet_id = "${var.node_subnet}"
  associate_public_ip_address = true
  vpc_security_group_ids = ["${aws_security_group.bastion.id}"]
  tags {
    Name = "bastion-${count.index}"
    Role = "bastion"
  }

  connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("~/wadeITTLPC6.pem")}"
  }

  provisioner "file" {
      source      = "terraform-inventory.linux"
      destination = "terraform-inventory"
  }
  provisioner "remote-exec" {
      inline = [
        "chmod +x /home/ubuntu/terraform-inventory",
      ]
  }
}
output "bastion_ip" {
  value = "${aws_instance.bastion.private_ip}"
}
