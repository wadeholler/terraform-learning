resource "aws_instance" "node" {
  count = "${var.node_count}"
  ami = "ami-8dc5c59b"
  instance_type = "t2.micro"
  key_name = "wadeITTLPC"
  subnet_id = "${var.node_subnet}"
  vpc_security_group_ids = ["${aws_security_group.node.id}"]
  associate_public_ip_address = true
  tags {
    Name = "node-${count.index}"
    Name = "node"
  }
}
output "node_ip" {
  value = "${join(",", aws_instance.node.*.private_ip)}"
}
