provider "aws" {
  region = "us-east-1"
  shared_credentials_file = "/Users/lholler/.aws/credentials"
  profile = "commercial"
}
resource "aws_instance" "example" {
  ami = "ami-d15a75c7"
  instance_type = "t2.micro"
  key_name = "wadeITTLPC"
  subnet_id = "subnet-ae5e5ecb"
  associate_public_ip_address = true

  user_data = <<-EOF
              #!/bin/bash
              echo "Hello, World" > index.html
              nohup busybox httpd -f -p "${var.server_port}" &
              EOF

# tagging during creation appears to not be available in govCloud
  tags {
    Name = "terraform-example"
  }
  vpc_security_group_ids = ["${aws_security_group.instance.id}"]

}
resource "aws_security_group" "instance" {
  name = "terraform-example-instance"
  vpc_id = "vpc-2af65753"
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "terraform-example-secgroup"
  }
}
variable "server_port" {
  description = "The port the server will use for HTTP requests"
  default = 8080
}
output "public_ip" {
  value = "${aws_instance.example.public_ip}"
}
