# Create a new load balancer
resource "aws_elb" "node_elb" {
  name               = "node-elb"
#  availability_zones = ["us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e" ]
  subnets            = ["${var.subnet10}", "${var.subnet11}", "${var.subnet12}", "${var.subnet13}"]
  internal           = true
  security_groups    = ["${aws_security_group.node.id}"]

#  access_logs {
#    bucket        = "foo"
#    bucket_prefix = "bar"
#    interval      = 60
#  }

  listener {
    instance_port     = 9000
    instance_protocol = "http"
    lb_port           = 9000
    lb_protocol       = "http"
  }

#  listener {
#    instance_port      = 8000
#    instance_protocol  = "http"
#    lb_port            = 443
#    lb_protocol        = "https"
#    ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
#  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:9000/minio/index.html"
    interval            = 30
  }

  instances                   = ["${aws_instance.node1.id}", "${aws_instance.node2.id}", "${aws_instance.node3.id}", "${aws_instance.node4.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name = "node-elb"
  }
}
