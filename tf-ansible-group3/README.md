# terraform-ansible-inventory demo stage 2

Modify stage 1 with customized AMIs build with packer

### bastion

AMI has ansible installed

### node

AMI has python installed as a dependency for being an
ansible target host

### handy commands

TF_KEY_NAME=private_ip ansible -i terraform-inventory -u ubuntu --private-key key -b -mping all
TF_KEY_NAME=private_ip ansible-playbook -i terraform-inventory -u ubuntu --private-key key minio-cluster/site.yml

