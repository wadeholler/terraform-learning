resource "aws_instance" "node1" {
#  count = "${var.node_count}"
  ami = "ami-8dc5c59b"
  instance_type = "${var.node_flavor}"
  key_name = "wadeITTLPC"
  subnet_id = "${var.subnet10}"
  vpc_security_group_ids = ["${aws_security_group.node.id}"]
  associate_public_ip_address = true
  tags {
    #Name = "node-${count.index}"
    Name = "node1"
    Role = "node"
    Agent = "telegraf"
  }
}
resource "aws_ebs_volume" "volnode1" {
    availability_zone = "us-east-1b"
    size = 250
    type = "gp2"
    tags {
        Name = "volnode1"
    }
}
resource "aws_volume_attachment" "volatnode1" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.volnode1.id}"
  instance_id = "${aws_instance.node1.id}"
}
output "node1_ip" {
  #value = "${join(",", aws_instance.node.*.private_ip)}"
  value = "${aws_instance.node1.private_ip}"
}
