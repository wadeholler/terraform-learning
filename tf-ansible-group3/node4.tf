resource "aws_instance" "node4" {
#  count = "${var.node_count}"
  ami = "ami-8dc5c59b"
  instance_type = "${var.node_flavor}"
  key_name = "wadeITTLPC"
  subnet_id = "${var.subnet13}"
  vpc_security_group_ids = ["${aws_security_group.node.id}"]
  associate_public_ip_address = true
  tags {
    #Name = "node-${count.index}"
    Name = "node4"
    Role = "node"
    Agent = "telegraf"
  }
}
resource "aws_ebs_volume" "volnode4" {
    availability_zone = "us-east-1e"
    size = 250
    type = "gp2"
    tags {
        Name = "volnode4"
    }
}
resource "aws_volume_attachment" "volatnode4" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.volnode4.id}"
  instance_id = "${aws_instance.node4.id}"
}
output "node4_ip" {
  #value = "${join(",", aws_instance.node.*.private_ip)}"
  value = "${aws_instance.node4.private_ip}"
}
