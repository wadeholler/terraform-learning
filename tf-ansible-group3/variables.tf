variable "node_count" {
  description = "The number of nodes to launch"
  default = 2
}
variable "bastion_count" {
  description = "The number of bastion nodes to launch"
  default = 1
}
variable "influxdb_count" {
  description = "The number of influxdb nodes to launch"
  default = 1
}
variable "bastion_flavor" {
  description = "The flavor of the bastion node"
  default = "c4.2xlarge"
}
variable "influxdb_flavor" {
  description = "The flavor of the influxdb node"
  default = "c4.2xlarge"
}
variable "node_flavor" {
  description = "The flavor of the nodes"
  default = "c4.2xlarge"
}
variable "subnet10" {
  description = "The subnet to launch nodes into"
  default = "subnet-4bb62667"
}
variable "subnet11" {
  description = "The subnet to launch nodes into"
  default = "subnet-b52d66fd"
}
variable "subnet12" {
  description = "The subnet to launch nodes into"
  default = "subnet-c558d09f"
}
variable "subnet13" {
  description = "The subnet to launch nodes into"
  default = "subnet-6681815a"
}
