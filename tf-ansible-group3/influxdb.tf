resource "aws_instance" "influxdb" {
  count = "${var.influxdb_count}"
  ami = "ami-a5c7c7b3"
  instance_type = "${var.influxdb_flavor}"
  key_name = "wadeITTLPC"
  subnet_id = "${var.subnet10}"
  associate_public_ip_address = true
  vpc_security_group_ids = ["${aws_security_group.influxdb.id}"]
  tags {
    Name = "influxdb-${count.index}"
    Role = "influxdb"
    Agent = "telegraf"
  }

}
output "influxdb_ip" {
  value = "${aws_instance.influxdb.private_ip}"
}
