variable "node_count" {
  description = "The number of nodes to launch"
  default = 2
}
variable "bastion_count" {
  description = "The number of bastion nodes to launch"
  default = 1
}
variable "node_subnet" {
  description = "The subnet to launch nodes into"
  default = "subnet-ae5e5ecb"
}
