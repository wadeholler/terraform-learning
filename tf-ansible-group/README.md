# terraform-ansible-inventory demo stage 1

## Introduction

Provision the following with teraform:

1. A bastion-ssh host
2. A variable number of nodes
3. Configure ansible on the bastion-ssh host
4. Copy in terraform-inventory
5. POST LAUNCH - copy in terraform.tfstate
6. POST LAUNCH - copy in {}.pem

### bastion-ssh host

Purpose: A bastion-ssh host to act as the entry point and
ansible deployment host

Security: Accessible from the internet via ssh; ssh via
the provided {} pem file

### node hosts

Purpose: A variable number nodes that can be ssh'd to from
the bastion-ssh host via the provided pem file

### pre-reqs

1. A VPC, and subnet to launch into
2. The VPC should have IGW and default route to IGW

### POST COMMANDS

copy in tfstate

	scp -i /Users/lholler/Google\ Drive/wadeITTLPC6.pem terraform.tfstate ubuntu@{BASTION_PUB_IP}:/home/ubuntu/.

copy in {}.pem

	scp -i /Users/lholler/Google\ Drive/wadeITTLPC6.pem /Users/lholler/Google\ Drive/wadeITTLPC6.pem ubuntu@{BASTION_PUB_IP}:/home/ubuntu/.
