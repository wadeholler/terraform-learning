resource "aws_instance" "node" {
  count = "${var.node_count}"
  ami = "ami-d15a75c7"
  instance_type = "t2.micro"
  key_name = "wadeITTLPC"
  subnet_id = "${var.node_subnet}"
  vpc_security_group_ids = ["${aws_security_group.node.id}"]
  associate_public_ip_address = true
  tags {
    Name = "node-${count.index}"
    Name = "node"
  }
  connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("~/wadeITTLPC6.pem")}"
  }
  provisioner "file" {
      source      = "scripts"
      destination = "scripts"
  }
  provisioner "remote-exec" {
      inline = [
        "chmod +x /home/ubuntu/scripts/node-install.sh",
        "/home/ubuntu/scripts/node-install.sh",
      ]
  }
}
output "node_ip" {
  value = "${join(",", aws_instance.node.*.private_ip)}"
}
