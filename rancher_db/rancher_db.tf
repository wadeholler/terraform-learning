module "rds" {
    source = "terraform-aws-modules/rds/aws"

    identifier = "rancherdb"
    engine            = "mysql"
    engine_version    = "5.7.17"
    instance_class    = "db.t2.large"
    allocated_storage = 200
    storage_encrypted = false

    name     = "rancherdb"
    username = "rancher"
    password = "rancherSecure01234"
    port     = "3306"

    vpc_security_group_ids = ["${aws_security_group.rancher-db-sg.id}"]

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"

    backup_retention_period = 0

    subnet_ids = ["subnet-efdfe98a", "subnet-b89bbfcf"]

    final_snapshot_identifier = "rancherdb"

    family = "mysql5.7"

    parameters = [
      {
        name = "max_allowed_packet"
        value = "33554432"
      }
    ]
}

#resource "aws_db_parameter_group" "rancher-params" {
#  name   = "rancher-pg"
#  family = "mysql5.7"
#
#  parameter = {
#    name = "character_set_client"
#    value = "utf8"
#  }
#  parameter = {
#    name = "character_set_server"
#    value = "utf8"
#  }
#  parameter = {
#    name = "max_packet_size"
#    value = "33554432"
#  }
#  parameter = {
#    name = "innodb_log_file_size"
#    value = "268435456"
#  }
#  parameter = {
#    name = "innodb_file_per_table"
#    value = "1"
#  }
#  parameter = {
#    name = "innodb_buffer_pool_size"
#    value = "2147483648"
#  }
#}
resource "aws_security_group" "rancher-db-sg" {
  name        = "rancher-db-sg"
  description = "Allow rancher mysql"
  vpc_id      = "vpc-c8ff5aad"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
